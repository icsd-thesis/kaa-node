
# dependencies - apt-get
sudo apt-get install -y wget curl net-tools ca-certificates software-properties-common git
sudo apt-get install -y iptables-persistent ufw

# make, g++ && dirmngr
sudo apt-get install -y make g++ dirmngr

# Java SE jdk-8u211 -

# Java (< April 16)
# sudo add-apt-repository -y ppa:webupd8team/java
# sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
# sudo apt-get update
# sudo apt-get install -y oracle-java8-installer
# java -version

# Java (> April 16 update)
# wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" https://download.oracle.com/otn-pub/java/jdk/8u191-b12/2787e4a523244c269598db4e85c51e0c/jdk-8u191-linux-x64.tar.gz
wget -O oracle-jdk-8u211.tar.gz https://www.dropbox.com/s/cb1spv5pdncx9lc/oracle-jdk-8u211.tar.gz?dl=0
sudo mkdir /usr/local/oracle-java-8
sudo tar -zxf oracle-jdk-8u211.tar.gz -C /usr/local/oracle-java-8
sudo update-alternatives --install /usr/bin/java java /usr/local/oracle-java-8/jdk1.8.0_211/bin/java 100

# python2.7.10 (2.7.10 for Cassandra-dependency)
cd assets
tar -xvf Python-2.7.10.tgz
cd Python-2.7.10
./configure --prefix /usr/local/lib/python2.7.10 --enable-ipv6
make
sudo make install
/usr/local/lib/python2.7.10/bin/python -V
cd ../..

# python-support
sudo apt install -f
sudo dpkg -i assets/python-support_1.0.15_all.deb

# !!
# tested on presentation server on April 17, 1019 (Debian)
