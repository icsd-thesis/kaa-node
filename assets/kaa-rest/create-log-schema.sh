# complete ctl filtering pipeline
grep id tmpctlid | sed 's/"//g' | rev | cut -d' ' -f1 | rev | sed 's/,//' > diswatineed

# store temp pass in shell variable
tctlid=$(head -n 1 diswatineed)

# clean-up
rm tmpctlid diswatineed

# node-conf : create apllication ctl
curl -v -S -u tendev:adminadmin -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{  "version": 1,  "applicationId": "1",  "name": "logschema",  "ctlSchemaId": "'$tctlid'"}' 'http://185.244.128.27:8080/kaaAdmin/rest/api/saveLogSchema' | python -mjson.tool

# log original temp pass
# echo `date "+%D"`, `date "+%T"` - ctlSchemaId : $tctlid >> assets/temp-passwords-origin

# release tpass
unset tctlid
